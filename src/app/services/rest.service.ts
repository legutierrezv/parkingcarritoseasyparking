import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private _http: HttpClient) { }

  getBrands() {
    return this._http.get(`${environment.api}/vehicle-brand/list`);
  }

  getBrand(id: number) {
    return this._http.get(`${environment.api}/vehicle-brand/${id}`);
  }

  getSlots() {
    return this._http.get(`${environment.api}/slot/list`);
  }

  getSlot(id: number) {
    return this._http.get(`${environment.api}/slot/${id}`);
  }

  getTypeDocuments() {
    return this._http.get('assets/data/type_document.json');
  }
}
