import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  remerber : boolean = false;
  user: any = {
    email: '',
    password: ''
  };

  constructor(private _router: Router) {
    localStorage.getItem('email') ? this.remerber = true : this.remerber = false;
    if(this.remerber) {
      this.user.email = localStorage.getItem('email');
    }
   }

  ngOnInit(): void {
  }

  login(){    
    this.remerber ? localStorage.setItem('email', this.user.email) : localStorage.removeItem('email');
    this._router.navigate(['/admin']);
  }
}
