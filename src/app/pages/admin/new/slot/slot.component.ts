import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/services/rest.service';

@Component({
  selector: 'app-slot',
  templateUrl: './slot.component.html',
  styleUrls: ['./slot.component.scss']
})
export class SlotComponent implements OnInit {

  brands : any[] = [];
  slots : any[] = [];
  typeDocument: any[] = [];
  selectSlot : any;
  newVehicle : boolean = false;
  constructor(private _rest : RestService) { 
    this._rest.getBrands().subscribe((data : any) => {
      this.brands = data;
    });
    this._rest.getSlots().subscribe((data : any) => {
      this.slots = data;
    });
    this._rest.getTypeDocuments().subscribe((data : any) => {
      this.typeDocument = data;
    });
  }

  ngOnInit(): void {
  }

  selectSlotM(slot : any) {
   this.selectSlot = slot;
  }
}
